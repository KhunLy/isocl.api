﻿using DemoApi.BLL.Services;
using DemoApi.DTO;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace DemoApi.Hubs
{
    public class ChatHub: Hub
    {
        private static Dictionary<string, string> _users = new Dictionary<string,string>();

        private readonly TokenService tService;

        public ChatHub(TokenService tService)
        {
            this.tService = tService;
        }

        public void SendMessage(MessageDTO message)
        {
            message.Auteur = _users[Context.ConnectionId];
            
            Clients.All.SendAsync("newMessage", message);
        }

        public override Task OnConnectedAsync()
        {
            var token = Context.GetHttpContext().Request.Query["access_token"];
            var user = tService.Validate(token);
            _users.Add(Context.ConnectionId, user.FindFirst(c => c.Type == ClaimTypes.NameIdentifier)?.Value);
            return base.OnConnectedAsync();
        }

        public override Task OnDisconnectedAsync(Exception exception)
        {
            _users.Remove(Context.ConnectionId);
            return base.OnDisconnectedAsync(exception);
        }

    }

}
