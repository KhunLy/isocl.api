﻿using DemoApi.BLL.BusinessObjects;
using DemoApi.BLL.Services;
using DemoApi.DTO;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DemoApi.Controllers
{
    [Route("api")]
    [ApiController]
    public class SecurityController : ControllerBase
    {
        private readonly UserService _userService;
        private readonly TokenService _tokenService;

        public SecurityController(UserService userService, TokenService tokenService)
        {
            _userService = userService;
            _tokenService = tokenService;
        }

        [Produces(typeof(UserDTO))]
        [HttpPost("Login")]
        public IActionResult Login(LoginDTO dto)
        {
            UserBO user = _userService.GetByUsernameAndPassword(dto.Username, dto.Password);
            if(user is null)
            {
                return BadRequest("Bad credentials");
            }
            return Ok(new UserDTO { Id = user.Id, Username = user.Username, Token = _tokenService.Create(user.Username)});
        }
    }
}
