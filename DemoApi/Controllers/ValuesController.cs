﻿using DemoApi.Attributes;
using DemoApi.DTO;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DemoApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        [ApiAuthorize]
        [HttpGet]
        [Produces(typeof(CustomDTO))]
        public IActionResult Get()
        {
            return Ok(new CustomDTO { Value = "SECRET" });
        }
    }
}
