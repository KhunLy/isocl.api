﻿namespace DemoApi.DTO
{
    public class MessageDTO
    {
        public string Content { get; set; }
        public string Auteur { get; set; }
    }
}
