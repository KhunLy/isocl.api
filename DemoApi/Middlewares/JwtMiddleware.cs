﻿using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;
using System.Linq;
using System.Security.Claims;
using DemoApi.BLL.Services;

namespace DemoApi.Middlewares
{
    public class JwtMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly TokenService _tokenService;

        public JwtMiddleware(RequestDelegate next, TokenService tokenService)
        {
            _next = next;
            _tokenService = tokenService;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            string token =
                context.Request.Headers.FirstOrDefault(h => h.Key == "Authorization").Value
                .FirstOrDefault(v => v.StartsWith("Bearer "))?.Replace("Bearer ", string.Empty);
            if(token != null)
            {
                context.User = _tokenService.Validate(token);
                
            }
            await _next(context);
        }

    }
}
