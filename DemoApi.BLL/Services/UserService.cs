﻿using DemoApi.BLL.BusinessObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoApi.BLL.Services
{
    public class UserService
    {
        public static IEnumerable<UserBO> _users = new List<UserBO>()
        {
            new UserBO { Id = 1, Username = "Khun", Password = "test1234=" },
            new UserBO { Id = 2, Username = "Yves", Password = "test1234=" },
            new UserBO { Id = 3, Username = "Julien", Password = "test1234=" },
            new UserBO { Id = 4, Username = "Cedric", Password = "test1234=" },
        };

        public UserBO GetByUsernameAndPassword(string username, string password)
        {
            return _users.FirstOrDefault(u => u.Username.ToLower() == username.ToLower() && u.Password == password);
        }
    }
}
