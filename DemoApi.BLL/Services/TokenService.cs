﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace DemoApi.BLL.Services
{
    public class JwtConfig
    {
        public string Signature { get; set; } 
    }

    public class TokenService
    {
        private readonly JwtConfig _config;
        private readonly JwtSecurityTokenHandler _handler;

        public TokenService(JwtConfig config, JwtSecurityTokenHandler handler)
        {
            _config = config;
            _handler = handler;
        }

        public string Create(string username)
        {
            JwtSecurityToken jwtSecurityToken = new JwtSecurityToken(null, null, GetClaims(username), null, null, new SigningCredentials(GetSigningKey(), SecurityAlgorithms.HmacSha256));
            return _handler.WriteToken(jwtSecurityToken);
        }

        public ClaimsPrincipal Validate(string token)
        {
            try
            {
                return _handler.ValidateToken(token, GetValidationParameters(), out SecurityToken securityToken);
            }
            catch (Exception)
            {
                return null;
            }
        }

        private TokenValidationParameters GetValidationParameters()
        {
            return new TokenValidationParameters
            {
                ValidateIssuer = false,
                ValidateAudience = false,
                ValidateLifetime = false,
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = GetSigningKey()
            };
        }

        private SecurityKey GetSigningKey()
        {
            return new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config.Signature));
        }

        private IEnumerable<Claim> GetClaims(string username)
        {
            yield return new Claim(ClaimTypes.NameIdentifier, username);
            yield return new Claim(ClaimTypes.Role, "ADMIN");
        }
    }
}
